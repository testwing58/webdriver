package test;

import java.util.concurrent.TimeUnit;
import java.util.Scanner; 

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.LogInPage;
import pages.HomePage;
import pages.SearchPageResult;



public class TestHomePage {
	
	WebDriver driver;
	
	LogInPage objLogInPage;
	HomePage  objHomePage;
	SearchPageResult objSearchPageResult;
	
	
	@BeforeClass
	public void setup(){
		  
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.get("https://jira.atlassian.com/secure/Dashboard.jspa");
	  /* User logs-in to Jira page */
		// User opens Home Page
		objHomePage = new HomePage(driver);
		
		/*String HomePage_TEXT = objHomePage.verifyHOMEPAGE();
		Assert.assertEquals(HomePage_TEXT, "Log In");
		objHomePage.clickLOGIN();
		
		// User goes to Login Page
		objLogInPage = new LogInPage(driver);
		String LOGINPAGE_TEXT = objLogInPage.verifyPresentPage();
		Assert.assertEquals(LOGINPAGE_TEXT, "Sign up");
		
		// Verify Login Successfully.
		objLogInPage.SignInProcess("testwing58@gmail.com", "abc_12Abc");
		String CREATEBUTTON_TEXT = objHomePage.verifyLoginSuccessfully();
		Assert.assertEquals(CREATEBUTTON_TEXT, "Create");*/
	 
	 }
	@Test
	public void test_Search_Issue_Page_Appear(){
		Scanner mdethod_to_search = new Scanner(System.in); 
		System.out.println("Please choose the kind of searching - BUG ID, or TEXT");
		System.out.println("Enter 'text' if you don't remember BUG-ID, and enter 'id' if you remmeber");
		String choose_data_to_search  = mdethod_to_search.nextLine();
		
		objSearchPageResult = new SearchPageResult(driver);
		switch (choose_data_to_search){
		
		case "text":
			Scanner search_string = new Scanner(System.in);
			System.out.println("Please enter the string to search: ");
			String strSearch  = search_string.nextLine();
			objHomePage.search_issue_process(strSearch);
			
			
			String Searchissue_TEXT = objSearchPageResult.getSearchquery();
			Assert.assertEquals(Searchissue_TEXT, "Search");
            break;
            
		case "id":
			Scanner search_id = new Scanner(System.in);
			System.out.println("Please enter the Bug ID to search: ");
			String strID  = search_id.nextLine();
			objHomePage.search_issue_process(strID);
			
			String BUGID_TEXT = objSearchPageResult.getBudIDTitle();
			Assert.assertEquals(BUGID_TEXT, strID.toUpperCase());
            break;
		
		}
			

		
	}
	
	@AfterClass
	public void CloseBrowserAndQuitSelenium(){
		driver.close();
		driver.quit();
	}
	
		
	
	
	

	
}
