package test;

import java.awt.AWTException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.BugDetailPage;
import pages.HomePage;
import pages.LogInPage;

public class TestBugDetailPage {
	private String url="https://https://jira.atlassian.com/browse/";
	WebDriver driver;
	
	LogInPage objLogInPage;
	HomePage  objHomePage;
	BugDetailPage objBugDetailPage;
	
	@BeforeClass
	public void setup(){
	  
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.get("https://jira.atlassian.com/secure/Dashboard.jspa");
	  /* User logs-in to Jira page */
		// User opens Home Page
		objHomePage = new HomePage(driver);
		
		String HomePage_TEXT = objHomePage.verifyHOMEPAGE();
		Assert.assertEquals(HomePage_TEXT, "Log In");
		objHomePage.clickLOGIN();
		
		// User goes to Login Page
		objLogInPage = new LogInPage(driver);
		String LOGINPAGE_TEXT = objLogInPage.verifyPresentPage();
		Assert.assertEquals(LOGINPAGE_TEXT, "Sign up");
		
		// Verify Login Successfully.
		objLogInPage.SignInProcess("testwing58@gmail.com", "abc_12Abc");
		String CREATEBUTTON_TEXT = objHomePage.verifyLoginSuccessfully();
		Assert.assertEquals(CREATEBUTTON_TEXT, "Create");
	 
	 }
	
	@Test
	public void test_UpdateExsitingIssue() throws InterruptedException, AWTException{
		objBugDetailPage = new BugDetailPage(driver);
		// Enter bug id into console to go BUG DEtail page.
		Scanner bug_id = new Scanner(System.in); 
		System.out.println("Please enter Bug ID you want to update: ");
		String strbug_id = bug_id.nextLine();
		driver.get("https://jira.atlassian.com/browse/"+strbug_id);
		
		// Verify Bug detail page 
		String BUGID_TEXT = objBugDetailPage.getBUGID();
		Assert.assertEquals(BUGID_TEXT, strbug_id.toUpperCase());
		//Assert.assertEquals(BUGID_TEXT, "TST-61425");
		
		// Update existing bug - add new attached file.
		
		objBugDetailPage.uploadATTACHFILE("C:\\data2.xls");
		
		
		
	}
	
	@AfterClass
	public void CloseBrowserAndQuitSelenium(){
		driver.close();
		driver.quit();
	}
	
	
}
