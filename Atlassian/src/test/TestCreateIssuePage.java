package test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import jxl.*;
import jxl.read.biff.BiffException;
import pages.CreateIssuePage;
import pages.HomePage;
import pages.LogInPage;
import pages.BugDetailPage;

public class TestCreateIssuePage {
	
	WebDriver driver;
	
	LogInPage objLogInPage;
	HomePage  objHomePage;
	CreateIssuePage objCreateIssuePage;
	BugDetailPage objBugDetailPage;
	
	@BeforeClass
	public void setup(){
		  
	  driver = new FirefoxDriver();
	  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  driver.get("https://jira.atlassian.com/secure/Dashboard.jspa");
	  /* User logs-in to Jira page */
		// User opens Home Page
		objHomePage = new HomePage(driver);
		
		String HomePage_TEXT = objHomePage.verifyHOMEPAGE();
		Assert.assertEquals(HomePage_TEXT, "Log In");
		objHomePage.clickLOGIN();
		
		// User goes to Login Page
		objLogInPage = new LogInPage(driver);
		String LOGINPAGE_TEXT = objLogInPage.verifyPresentPage();
		Assert.assertEquals(LOGINPAGE_TEXT, "Sign up");
		
		// Verify Login Successfully.
		objLogInPage.SignInProcess("testwing58@gmail.com", "abc_12Abc");
		String CREATEBUTTON_TEXT = objHomePage.verifyLoginSuccessfully();
		Assert.assertEquals(CREATEBUTTON_TEXT, "Create");
	 
	 }
	
	
	@Test
	public void test_Create_Issue_Page_Appear() throws BiffException, IOException, InterruptedException{
		/*1. Go to Creation Issue Page*/
		objHomePage.creation_issue_process();
				
		objCreateIssuePage = new CreateIssuePage(driver);
		String CREATE_ISSUE_PAGE_TITLE = objCreateIssuePage.getCreateIssuePageTitle();
		Assert.assertEquals(CREATE_ISSUE_PAGE_TITLE, "Create Issue");
		
		/*2. Define Cell info */
		Workbook workbook = Workbook.getWorkbook(new File("C:\\data2.xls"));
		Sheet sheet = workbook.getSheet("data");
		String strsummary = sheet.getCell(1, 4).getContents();
		String strdescription = sheet.getCell(1, 5).getContents();
		String issuetype = sheet.getCell(1, 2).getContents();
		String priority = sheet.getCell(1, 3).getContents();
		String attachfilepath = sheet.getCell(1, 6).getContents();
		
		/*3. Enter into to creation Issue form*/
		objCreateIssuePage.creat_issue_process(strsummary, strdescription, issuetype, priority);
		workbook.close();
		//Verify issue is created successfully.
		objHomePage.verifyCreateBugSuccessfully_Messsage();
		
	}
	
	@AfterClass
	public void CloseBrowserAndQuitSelenium(){
		driver.close();
		driver.quit();
	}
		

}
