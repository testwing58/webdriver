package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LogInPage {
	
	WebDriver driver ;
	
	public LogInPage(WebDriver driver){

        this.driver = driver;
    }
	
	// Element Locator
	By USERNAME_FIELD = By.id("username");
	By PASSWORD_FIELD = By.id("password");
	By SIGNIN_BUTTON = By.id("login-submit");
	By SIGNUP_TEXT = By.xpath("//a[@id='signup']");
	
    public String verifyPresentPage(){
    	 
        return driver.findElement(SIGNUP_TEXT).getText();
    
       }
	public void enterUserName(String strUserName){
		driver.findElement(USERNAME_FIELD).sendKeys(strUserName);
	}
	
	public void enterPassword(String strPassword){
		driver.findElement(PASSWORD_FIELD).sendKeys(strPassword);
	}
	
	public void clickSUBMIT(){
		driver.findElement(SIGNIN_BUTTON).click();
	}
	
	public  void SignInProcess(String strUserName, String strPassword){
		this.enterUserName(strUserName);
		this.enterPassword(strPassword);
		this.clickSUBMIT();
		
	}

}
