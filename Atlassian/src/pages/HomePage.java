package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomePage{
	
	WebDriver driver ;
	
	public HomePage(WebDriver driver){

        this.driver = driver;
    }
	
	// Set-up element locator
	By LOGIN_BUTTON = By.xpath("//*[contains(text(),'Log In')]");
	By CREATE_ISSUE_BUTTON = By.id("create_link");
	By SEARCH_FIELD = By.id("quickSearchInput");
	By TITLE_TEXT = By.xpath("//a[@class='aui-nav-link login-link']");
    By CREATION_SUCCESS_MESSAGE = By.className("aui-message aui-message-success success closeable shadowed");
	// configure method
	public void createIssue(){
		driver.findElement(CREATE_ISSUE_BUTTON).click();
	}
	
	public String verifyLoginSuccessfully(){
		return driver.findElement(CREATE_ISSUE_BUTTON).getText();
	}
	
	public String verifyHOMEPAGE(){
		 
	     return driver.findElement(TITLE_TEXT).getText();
	 
	    }
	
	public void verifyCreateBugSuccessfully_Messsage(){
		driver.findElement(CREATION_SUCCESS_MESSAGE).isDisplayed();
	}
	public void switch_window(){
		String winHandleBefore = driver.getWindowHandle();
	    driver.switchTo().window(winHandleBefore);
	}
	public void searchField(String strSearch){
		driver.findElement(SEARCH_FIELD).sendKeys(strSearch + Keys.ENTER);
	}
	
	public void clickLOGIN(){
		driver.findElement(LOGIN_BUTTON).click();
	}
	
	public void creation_issue_process(){
		this.createIssue();
		this.switch_window();
	}
	
	public void search_issue_process(String issue){
		this.searchField(issue);
	}
}
