package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchPageResult {
	
	WebDriver driver ;
	
	public SearchPageResult (WebDriver driver){
		this.driver = driver;
	}
	
	// Element Locator
	By SEARCH_QUERY_FIELD = By.xpath("//h1[@class='search-title']");
	By BUG_ID_TITLE = By.xpath("//li/a[@id='key-val']");
	
	public String getSearchquery(){
		return driver.findElement(SEARCH_QUERY_FIELD).getText();
	}
	
	public String getBudIDTitle(){
		return driver.findElement(BUG_ID_TITLE).getText();
	}

}
