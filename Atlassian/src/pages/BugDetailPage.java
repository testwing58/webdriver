package pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BugDetailPage {
	
	WebDriver driver ;
	
	public BugDetailPage (WebDriver driver){
		this.driver = driver;
	}
	
	// Element locator
	By BUG_ID = By.id("key-val");
	By BUG_NAME = By.xpath("//hl[@id='summary-val']");
	By EDIT_BUTTON = By.id("edit-issue");
	By ATTACHMENT_BUTTON = By.xpath("//label[contains(.,'Select files')]");
	By ATTACHED_FILES = By.xpath("//dt[@class='attachment-title']");
	
	public void clickEDIT_Button(){
		driver.findElement(EDIT_BUTTON).click();
	}
	
	public void uploadATTACHFILE(String strpath) throws InterruptedException, AWTException{
		WebElement attach = driver.findElement(ATTACHMENT_BUTTON);
		attach.click();
		Thread.sleep(5);
		StringSelection ss = new StringSelection(strpath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	public WebElement getAttachedFileName (){
		return driver.findElement(ATTACHED_FILES);
	}
		
	public String getBUGNAME(){
		return driver.findElement(BUG_NAME).getText();
	}
	public String getBUGID(){
		return driver.findElement(BUG_ID).getText();
	}

}
