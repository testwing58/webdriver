package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;



public class CreateIssuePage {

	WebDriver driver ;
	
	public CreateIssuePage (WebDriver driver){
		this.driver = driver;
	}
	
	// Element locator
	By TITLE = By.xpath("//h2[@title='Create Issue']");
	By ISSUETYPE_FIELD = By.id("issuetype-field");
	By SUMMARY_TEXTBOX = By.id("summary");
	By PRIORITY_FIELD = By.id("priority-field");
	By COMPONENT_FIELD = By.id("components-textarea");
	By AFFECT_VERSION_FIELD = By.id("versions-textarea");
	By ENVIRONMENT_TEXTBOX = By.id("environment");
	By DESCRIPTION_TEXTBOX = By.id("description");
	By ATTACHMENT = By.xpath("//label[contains(.,'Select files')]");
	By SUBMIT_BUTTON = By.id("create-issue-submit");
	
	//configure locator
	
	public String getCreateIssuePageTitle(){
		return driver.findElement(TITLE).getText();
	}
	public void clickCreate_Button(){
		Actions action = new Actions(driver);
		WebElement CREATE_ISSUE_BUTTON = driver.findElement(SUBMIT_BUTTON);
		WebElement BODY_LAYOUT = driver.findElement(By.className("aui-layout aui-theme-default page-type-dashboard"));
		BODY_LAYOUT.click();
		action.moveToElement(CREATE_ISSUE_BUTTON).clickAndHold().build().perform();
	}
	
	
	public void typeSUMMARY(String strsummary){
		driver.findElement(SUMMARY_TEXTBOX).sendKeys(strsummary); 
	}
	
	public void typeENVIRONMENT(String strenvironment){
		driver.findElement(ENVIRONMENT_TEXTBOX).sendKeys(strenvironment); 
	}
	
	public void typeDESCRIPTION(String strdescription){
		driver.findElement(DESCRIPTION_TEXTBOX).sendKeys(strdescription); 
	}
	
	public void chooseISSUETYPE(String strISSUETYPE) throws InterruptedException{
		WebElement issuetype_dropdown = driver.findElement(ISSUETYPE_FIELD);
		issuetype_dropdown.click();
		issuetype_dropdown.sendKeys(strISSUETYPE + Keys.ENTER);
		Thread.sleep(3);
		//issuetype_dropdown.sendKeys((Keys.ALT )+ (Keys.SHIFT + "S")) ;
	}
	public void choosePRIORITY(String strPRIORITY){
		WebElement priority_dropdown = driver.findElement(PRIORITY_FIELD);
		priority_dropdown.click();
		priority_dropdown.sendKeys(strPRIORITY);
	}
	public void chooseCOMPONENT(String strCOMPONENT){
		WebElement component_dropdown = driver.findElement(COMPONENT_FIELD);
		component_dropdown.click();
		component_dropdown.sendKeys(strCOMPONENT + Keys.ENTER);
	}
	public void chooseAFFECTVERSION(String strAFFECTVERSION){
		WebElement affect_version_dropdown = driver.findElement(AFFECT_VERSION_FIELD);
		affect_version_dropdown.click();
		affect_version_dropdown.sendKeys(strAFFECTVERSION + Keys.ENTER);
	}
	public void uploadATTACHFILE(String strpath) throws InterruptedException{
		WebElement attach = driver.findElement(ATTACHMENT);
		attach.click();
		
		
	}
	
	public void creat_issue_process(String strsummary, String strdescription, String issuetype,  String priority) throws InterruptedException
	
	{
		
		this.typeSUMMARY(strsummary);
		this.typeDESCRIPTION(strdescription);
		this.choosePRIORITY(priority);
		this.chooseISSUETYPE(issuetype);
		this.clickCreate_Button();
		
	}
	
}
